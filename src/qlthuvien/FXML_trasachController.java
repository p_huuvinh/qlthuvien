/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vinh
 */
public class FXML_trasachController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Statement statement;
    private ResultSet rs;
    private ObservableList<sach> list;
    @FXML
    public Button btn_close;
    private DatePicker dp_ngaymuon;
    @FXML
    private Button btn_nhap;
    @FXML
    private TextField tf_tensach,tf_ten,tf_tendocgia;
    @FXML
    private TextField tf_masach;
    @FXML
    private Button btn_themds; 
    @FXML
    private TableView table;
    /*************************
     * HANDLE EVENT
     ************************/
    @FXML
    public void handleaThoatAction(ActionEvent event) throws IOException {
            
            Stage stage = (Stage) btn_close.getScene().getWindow();
            stage.hide();
    }
     @FXML
    public void handleBtnThemAction(ActionEvent event) throws IOException, SQLException {
            rs = statement.executeQuery("select doc_gia.maDocGia,tenDocGia,sach.maSach,ngayMuon "
                    + "from sach inner join chi_tiet_phieu_muon on sach.maSach=chi_tiet_phieu_muon.maSach"
                    + "inner join phieu_muon on chi_tiet_phieu_muon.maPhieuMuon=phieu_muon.maPhieuMuon"
                    + "inner join doc_gia on phieu_muon.maDocGia = doc_gia.MaDocGia;");
           
    }   
     @FXML
    public void handlebtnNhapAction(ActionEvent event) throws IOException {
            
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            statement = QLThuVien.conn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(FXML_trasachController.class.getName()).log(Level.SEVERE, null, ex);
        }
     tf_masach.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                    tf_tendocgia.setText("");
                    try {
                       rs = statement.executeQuery("select tenDocGia "
                    + "from sach inner join chi_tiet_phieu_muon on sach.maSach=chi_tiet_phieu_muon.maSach "
                    + "inner join phieu_muon on chi_tiet_phieu_muon.maPhieuMuon=phieu_muon.maPhieuMuon "
                    + "inner join doc_gia on phieu_muon.maDocGia = doc_gia.MaDocGia "
                               + "where sach.maSach= '"+Integer.parseInt(tf_masach.getText())+"';");
                        while(rs.next())
                            tf_tendocgia.setText(rs.getString(1));
                       // System.out.println(rs_dg.getString(1));
                    } catch (SQLException ex) {
                        Logger.getLogger(FXML_muonsachController.class.getName()).log(Level.SEVERE, null, ex);
                    }                  
                }
            }
        });
    }    
    
}
