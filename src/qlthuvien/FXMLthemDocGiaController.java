/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vinh
 */
public class FXMLthemDocGiaController implements Initializable {
    @FXML
    private Button btn_close;
    @FXML
    private Button btn_nhap;
    @FXML
    private Button btn_nhap_lai;
    @FXML
    private TextField tf_ten_dg,tf_email,tf_dia_chi;
    @FXML
    private DatePicker dp_ngaysinh,dp_ngaylapthe;
//    @FXML
//    private ToggleGroup group_gioitinh;
    @FXML
    private RadioButton rbtn_nam, rbtn_nu;
    /* HANDLE BUTTON ACTION */
    @FXML
    public void handleaThoatAction(ActionEvent event) throws IOException {
            
            Stage stage = (Stage) btn_close.getScene().getWindow();
            stage.hide();
    }
    @FXML
    public void handleBtnNhapDGAction(ActionEvent event) throws SQLException{
        //connect db
        
        Statement statement = QLThuVien.conn.createStatement();
        if(rbtn_nam.isSelected())
            statement.executeUpdate("INSERT INTO `doc_gia`(`maDocGia`,`tenDocGia`,`diaChi`,`email`,"
                    + "`ngaySinh`,`ngayLapThe`,`gioiTinh`) values (NULL,'"+tf_ten_dg.getText()+"'"
                    + ",'"+tf_dia_chi.getText()+"','"+tf_email.getText()+"','"+dp_ngaysinh.getValue()+"','"+dp_ngaylapthe.getValue()+"',\"nam\");");
        else if(rbtn_nu.isSelected())
            statement.executeUpdate("INSERT INTO `doc_gia`(`maDocGia`,`tenDocGia`,`diaChi`,`email`,"
                    + "`ngaySinh`,`ngayLapThe`,`gioiTinh`) values (NULL,'"+tf_ten_dg.getText()+"'"
                    + ",'"+tf_dia_chi.getText()+"','"+tf_email.getText()+"','"+dp_ngaysinh.getValue()+"','"+dp_ngaylapthe.getValue()+"',\"nu\");");
//        ResultSet rs = statement.executeQuery("select maDocGia,tenDocGia,"
//                + "gioiTinh,diaChi,email,ngaySinh,NgayLapThe"
//                + " from doc_gia; ");
        //FXMLDocumentController.doc_gia_table();
        tf_ten_dg.setText("");
        tf_email.setText("");
        tf_dia_chi.setText("");
        dp_ngaysinh.setValue(LocalDate.now());
        dp_ngaylapthe.setValue(LocalDate.now());
        //System.out.println(dp_ngaysinh.getValue() + "  "+dp_ngaylapthe.getValue());
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ToggleGroup group_gioitinh = new ToggleGroup();
        rbtn_nam.setToggleGroup(group_gioitinh);
        rbtn_nu.setToggleGroup(group_gioitinh);
        rbtn_nam.setSelected(true);
        dp_ngaysinh.setValue(LocalDate.now());
        dp_ngaylapthe.setValue(LocalDate.now());
    }    
    
}
