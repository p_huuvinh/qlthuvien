/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;


import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Vinh
 */
public class QLThuVien extends Application {
   // private static Connection con;
    Scene scene;
     public static Connection conn;
     FXMLDocumentController tb;
    //private Button bt_addDG = (Button) scene.lookup("#bt_addDG");
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        scene = new Scene(root);
         
        stage.setTitle("Quản lý thư viện");
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, SQLException {
        DBConnection();
       
        launch(args);
    }
    private  static void DBConnection(){
         String databaseURL = "jdbc:mysql://localhost:3333/nhasach";
        String user = "root";
        String password = "1234";
        conn = null;
        try {
            conn = (Connection) DriverManager.getConnection(databaseURL, user, password);
            if (conn != null) {
                System.out.println("Connected to the database");
            }
        } catch (SQLException ex) {
            System.out.println("An error occurred. Maybe user/password is invalid");
            ex.printStackTrace();
        } 
    }
    
    
}