/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vinh
 */
public class FXML_SachController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    public Button btn_close;
    @FXML
    private TextField tf_tg;
    @FXML
    private TextField tf_ten_sach;
    @FXML
    private ComboBox<String> cb_theloai; 
    @FXML
    private DatePicker dp_namxb;
    @FXML
    private Button btn_nhap;
    @FXML
    private TextField tf_ten;
    @FXML
    private DatePicker dp_ngaymuon;
    @FXML
    private TextField tf_masach;
    @FXML
    private TextField tf_tensach;
    @FXML
    private Button btn_themds; 
    @FXML
    public void handleaThoatAction(ActionEvent event) throws IOException {
            
            Stage stage = (Stage) btn_close.getScene().getWindow();
            stage.hide();
    }
    public void handleBtnNhapSachAction(ActionEvent event) throws SQLException{
       // dp_namxb.setPromptText("dd-MM-yy");      
        Statement statement = QLThuVien.conn.createStatement();
        statement.executeUpdate("INSERT INTO `sach`(`maSach`,`tenSach`,`tacGia`,`namXB`,`theLoai`,`tinhTrang`) values (NULL,'"+tf_ten_sach.getText()
                +"','"+tf_tg.getText()+"','"+dp_namxb.getValue()+"','"+cb_theloai.getSelectionModel().getSelectedItem()+"',0);");
        tf_ten_sach.setText("");
        tf_tg.setText("");
        cb_theloai.getSelectionModel().select(1);
        System.out.println(cb_theloai.getSelectionModel().getSelectedItem());
    }
//    public void handleComboAction(ActionEvent event){
//        System.out.println(cb_theloai.geti);
//    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        cb_theloai.getItems().addAll("Khoa học viễn tưởng","Trinh Thám","Khoa học tự nhiên"
                ,"Khoa học xã hội","Công nghệ thông tin","Tiểu thuyết","Tham Khảo","Ngoại ngữ","Kinh tế");
        cb_theloai.getSelectionModel().select(0);
//       
    }    
    
}
