/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.util.Date;

/**
 *
 * @author Vinh
 */
public class doc_gia {
    public String tenDocGia, diaChi,gioiTinh,email;
    public int maDocGia;
    public Date ngaySinh, ngayLapThe;

    public doc_gia(int maDocGia,String tenDocGia,String gioiTinh, String diaChi, String email,  Date ngaySinh, Date ngayLapThe) {
        this.tenDocGia = tenDocGia;
        this.diaChi = diaChi;
        this.gioiTinh = gioiTinh;
        this.email=email;
        this.maDocGia = maDocGia;
        this.ngaySinh = ngaySinh;
        this.ngayLapThe = ngayLapThe;
    }

    public String getTenDocGia() {
        return tenDocGia;
    }

    public void setTenDocGia(String tenDocGia) {
        this.tenDocGia = tenDocGia;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public int getMaDocGia() {
        return maDocGia;
    }

    public void setMaDocGia(int maDocGia) {
        this.maDocGia = maDocGia;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public Date getNgayLapThe() {
        return ngayLapThe;
    }

    public void setNgayLapThe(Date ngayLapThe) {
        this.ngayLapThe = ngayLapThe;
    }
    
}
