/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 *
 * @author Vinh
 */
public class FXMLDocumentController implements Initializable {
    /* FXML COMPONENT INIT */
    public Button bt_tdg_thoat;
    private Button btn_muon_sach;
    private Button  btn_themDoc_gia;
    @FXML
    private Button btn_nhap_sach;
    @FXML
    private Button btn_nhap_lai_sach;
    @FXML
    private Button btn_close;
    @FXML
    private TextField tf_nha_xb;
    @FXML
    private TextField tf_tg;
    @FXML
    private TextField tf_ten_sach;
    @FXML
    private DatePicker dp_namxb;
    @FXML
    public  TableView table_sach,table_doc_gia;
    @FXML
    public  TableColumn<sach,String> col_tenSach;
    @FXML
    public  TableColumn<sach,String> col_tg;
    @FXML
    public  TableColumn<sach,Object>col_namXB;
    @FXML
    public  TableColumn<sach,Integer>col_maSach;
    @FXML
    public  TableColumn<sach,String>col_theLoai;
    @FXML
    public  TableColumn<doc_gia,String>col_tenDG;
    @FXML
    public  TableColumn<doc_gia,String>col_gioitinh;
    @FXML
    public  TableColumn<doc_gia,Integer>col_maDG;
    @FXML
    public  TableColumn<doc_gia,String>col_diachi;
    @FXML
    public  TableColumn<doc_gia,String>col_email;
    @FXML
    public  TableColumn<doc_gia,Object>col_ngay_sinh;
    @FXML
    public  TableColumn<doc_gia,Object>col_ngay_lapThe;
    @FXML
    private ChoiceBox<?> cb_the_loai;
   //private ObservableList<ObservableList> data;
    @FXML
    public void handleaddSachAction(ActionEvent event) throws IOException{
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLthemSach.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            //stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("Thêm sach");
            stage.setScene(new Scene(root1));  
            stage.show();
        
    }
    @FXML
    public void handleMuonSachAction(ActionEvent event) throws IOException{
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLmuonSach.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            //stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("Mượn sach");
            stage.setScene(new Scene(root1));  
            stage.show();
        
    }
    @FXML
        public void handleTraSachAction(ActionEvent event) throws IOException{
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLtraSach.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            //stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("Trả sách");
            stage.setScene(new Scene(root1));  
            stage.show();
        
    }
    public void handleaddDocGiaAction(ActionEvent event) throws IOException {
  
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLthemDocGia.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            //stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("Thêm đọc giả");
            stage.setScene(new Scene(root1));  
            stage.show();
    }
    @FXML
    public void handleaThoatAction(ActionEvent event) throws IOException {
            Stage stage = (Stage) btn_close.getScene().getWindow();
            stage.hide();
    }
    
    //BUTTON

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            sach_table();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            doc_gia_table();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    /* TABLE SACH VIEW */
    @FXML
    public void sach_table() throws SQLException
    {
        sach sach;
       // table_sach=new TableView<sach>();
        ObservableList<sach> list = FXCollections.observableArrayList() ;
        //list=FXCollections.observableArrayList();
        Statement statement = QLThuVien.conn.createStatement();
        ResultSet rs = statement.executeQuery("select maSach,tenSach,theLoai,tacGia,namXB"
                + " from sach; ");
        
        col_tenSach=new TableColumn<sach,String>("Tên sách");
        col_tenSach.setMinWidth(100);
        col_tenSach.setCellValueFactory(new PropertyValueFactory<sach,String>("tenSach"));
        col_maSach = new TableColumn<sach,Integer>("Mã sách");
        col_maSach.setMinWidth(50);
        col_maSach.setCellValueFactory(new PropertyValueFactory<sach,Integer>("maSach"));
        col_tg=new TableColumn<sach,String>(" Tác giả");
        col_tg.setMinWidth(100);
        col_tg.setCellValueFactory(new PropertyValueFactory<sach,String>("tacGia"));
        col_namXB=new TableColumn<sach,Object>("Năm xuất bản");
        col_namXB.setMinWidth(200);
        col_namXB.setCellValueFactory(new PropertyValueFactory<sach,Object>("namXB"));
        col_theLoai = new TableColumn<sach,String>("Thể loại");
        col_theLoai.setMinWidth(150);
        col_theLoai.setCellValueFactory(new PropertyValueFactory<sach,String>("theLoai"));
        //data = FXCollections.observableArrayList();
          table_sach.getColumns().addAll(col_maSach,col_tenSach,col_theLoai,col_tg,col_namXB);
        while(rs.next()){              
           sach = new sach(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5));                    
           list.add(sach);
        } 
         table_sach.setItems(list);
    }
    @FXML
    public void doc_gia_table() throws SQLException
    {
        doc_gia docgia;
       // table_sach=new TableView<sach>();
        ObservableList<doc_gia> list = FXCollections.observableArrayList() ;
        //list=FXCollections.observableArrayList();
        Statement statement = QLThuVien.conn.createStatement();
        ResultSet rs = statement.executeQuery("select maDocGia,tenDocGia,"
                + "gioiTinh,diaChi,email,ngaySinh,NgayLapThe"
                + " from doc_gia; ");
        
        col_tenDG=new TableColumn<doc_gia,String>("Tên đọc giả");
        col_tenDG.setMinWidth(100);
        col_tenDG.setCellValueFactory(new PropertyValueFactory<doc_gia,String>("tenDocGia"));
        col_maDG=new TableColumn<doc_gia,Integer>("Mã đọc giả");
        col_maDG.setMinWidth(50);
        col_maDG.setCellValueFactory(new PropertyValueFactory<doc_gia,Integer>("maDocGia"));
        col_gioitinh=new TableColumn<doc_gia,String>("Giới tính");
        col_gioitinh.setMinWidth(50);
        col_gioitinh.setCellValueFactory(new PropertyValueFactory<doc_gia,String>("gioiTinh"));
        col_diachi=new TableColumn<doc_gia,String>("Địa chỉ");
        col_diachi.setMinWidth(100);
        col_diachi.setCellValueFactory(new PropertyValueFactory<doc_gia,String>("diaChi"));
        col_email=new TableColumn<doc_gia,String>("Email");
        col_email.setMinWidth(100);
        col_email.setCellValueFactory(new PropertyValueFactory<doc_gia,String>("email"));
        col_ngay_sinh=new TableColumn<doc_gia,Object>(" Ngày sinh");
        col_ngay_sinh.setMinWidth(100);
        col_ngay_sinh.setCellValueFactory(new PropertyValueFactory<doc_gia,Object>("ngaySinh"));
        col_ngay_lapThe=new TableColumn<doc_gia,Object>(" Ngày lập thẻ");
        col_ngay_lapThe.setMinWidth(100);
        col_ngay_lapThe.setCellValueFactory(new PropertyValueFactory<doc_gia,Object>("ngayLapThe"));
        //data = FXCollections.observableArrayList();
          table_doc_gia.getColumns().addAll(col_maDG,col_tenDG,col_gioitinh,col_ngay_sinh,col_ngay_lapThe,col_diachi);
        while(rs.next()){              
           docgia = new doc_gia(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getDate(6),rs.getDate(7));                    
           list.add(docgia);
        } 
         table_doc_gia.setItems(list);
    }
}
