/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vinh
 */
public class FXML_muonsachController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Statement statement;
    private ResultSet rs_dg;
    private ObservableList<sach> list;
    @FXML
    public Button btn_close;
    @FXML
    private DatePicker dp_ngaymuon;
    @FXML
    private Button btn_nhap;
    @FXML
    private TextField tf_tensach,tf_ten,tf_madocgia;
    @FXML
    private TextField tf_masach;
    @FXML
    private Button btn_themds; 
    @FXML
    private TableView table;
    /*************************
     * HANDLE EVENT
     ************************/
    @FXML
    public void handleaThoatAction(ActionEvent event) throws IOException {
            
            Stage stage = (Stage) btn_close.getScene().getWindow();
            stage.hide();
    }
    public void handleBtnThemSachAction(ActionEvent event) throws SQLException{
        sach sach;
         
        ResultSet rs = statement.executeQuery("select maSach,tenSach,theLoai,tacGia,"
                + "namXB, tinhTrang from sach where maSach = '"+ Integer.parseInt(tf_masach.getText())+"';");
        while(rs.next()){
            
            if(!rs.getBoolean(6)){
                tf_tensach.setText(rs.getString(2));
                sach = new sach(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5));
                list.add(sach);
            }       
            else{
                tf_masach.setText("");
            }
        }       
        table.setItems(list);
    }
    public void handleBtnNhapSachAction(ActionEvent event) throws SQLException{
        ResultSet rs;
        int pmid=0;
        statement = QLThuVien.conn.createStatement();
        if(list.size()!=0){
            statement.executeUpdate("INSERT INTO `phieu_muon`(`maPhieuMuon`,"
                    + "`maDocGia`,`ngayMuon`) values (NULL,"
                    + "'"+Integer.parseInt(tf_madocgia.getText())+"','"+dp_ngaymuon.getValue()+"');");
            rs=statement.executeQuery("select MAX(maPhieuMuon) from phieu_muon;");
            while(rs.next())
              pmid=rs.getInt(1);
            for(sach s : list){               
                    statement.executeUpdate("INSERT INTO `chi_tiet_phieu_muon`"
                        + "(`maCTPhieuMuon`,`maSach`,`maPhieuMuon`) values ("
                        + "NULL,"+s.maSach+",'"+pmid+"');");
                    statement.executeUpdate("UPDATE `sach` SET tinhTrang = 1 "
                            + "where sach.maSach = "+s.maSach+";");
                    
            }
        }
    }
    public void table_sach(){
        TableColumn<sach,String> col_tenSach=new TableColumn<sach,String>("Tên sách");
        col_tenSach.setMinWidth(100);
        col_tenSach.setCellValueFactory(new PropertyValueFactory<sach,String>("tenSach"));
        TableColumn<sach,Integer> col_maSach = new TableColumn<sach,Integer>("Mã sách");
        col_maSach.setMinWidth(50);
        col_maSach.setCellValueFactory(new PropertyValueFactory<sach,Integer>("maSach"));
        TableColumn<sach,String>col_tg=new TableColumn<sach,String>(" Tác giả");
        col_tg.setMinWidth(100);
        col_tg.setCellValueFactory(new PropertyValueFactory<sach,String>("tacGia"));
        TableColumn<sach,Object> col_namXB=new TableColumn<sach,Object>("Năm xuất bản");
        col_namXB.setMinWidth(200);
        col_namXB.setCellValueFactory(new PropertyValueFactory<sach,Object>("namXB"));
        TableColumn<sach,String>col_theLoai = new TableColumn<sach,String>("Thể loại");
        col_theLoai.setMinWidth(150);
        col_theLoai.setCellValueFactory(new PropertyValueFactory<sach,String>("theLoai"));
        table.getColumns().addAll(col_maSach,col_tenSach,col_theLoai,col_tg,col_namXB);
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            statement = QLThuVien.conn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(FXML_muonsachController.class.getName()).log(Level.SEVERE, null, ex);
        }
       list = FXCollections.observableArrayList();
       table_sach();
       //ResultSet rs_dg;
       tf_madocgia.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    System.out.println("Textfield on focus");
                }
                else
                {
                     //ResultSet rs_dg = null ;
                    try {
                        rs_dg = statement.executeQuery("select tenDocGia "
                                + "from doc_gia where maDocGia='"+Integer.parseInt(tf_madocgia.getText())+"';");
                        while(rs_dg.next())
                            tf_ten.setText(rs_dg.getString(1));
                       // System.out.println(rs_dg.getString(1));
                    } catch (SQLException ex) {
                        Logger.getLogger(FXML_muonsachController.class.getName()).log(Level.SEVERE, null, ex);
                    }                  
                }
            }
        });
    }    
    
}
