/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
 import com.mysql.jdbc.Connection;
/**
 *
 * @author Vinh
 */
public class DBConnection {
   private static Connection con;
   private static String user;
   private static String url;
   private static String psswd;
   
   
   public static Connection getConnection() throws IOException, SQLException{
       con =null;
       Properties properties = new Properties();
        properties.load(new FileReader(new File("info.properties")));
            url = properties.getProperty("jdbc:mysql://localhost:3333/nhasach");
            user = properties.getProperty("root");
            psswd = properties.getProperty("1234");
            // driver register
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = (Connection) DriverManager.getConnection(url, user, psswd);
       return con;
   }
       public static void freeConnection() {
       try {     
           con.close();
       } catch (SQLException ex) {
           Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
}
