/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qlthuvien;

import java.util.Date;

/**
 *
 * @author Vinh
 */
public class sach {
    public final String  tenSach, theLoai,tacGia;
    public final Date namXB;
    public final int  maSach;

    public sach(int maSach,String tenSach, String theLoai, String tacGia, Date namXB ) {
        this.tenSach = tenSach;
        this.theLoai = theLoai;
        this.tacGia = tacGia;
        this.namXB = namXB;
        this.maSach = maSach;
    }

    public String getTenSach() {
        return tenSach;
    }

    public String getTheLoai() {
        return theLoai;
    }

    public String getTacGia() {
        return tacGia;
    }

    public Date getNamXB() {
        return namXB;
    }

    public int getMaSach() {
        return maSach;
    }

    
}
